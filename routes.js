const express = require("express");
const routes = express.Router();

const TreinoController = require("./controllers/TreinoController");
const ExercicioController = require("./controllers/ExercicioController");
const UsuarioController = require("./controllers/UsuarioController");
const TreinoHasExercicioController = require("./controllers/TreinoHasExercicioController");
const login = require("./middleware/login");

routes
  .get("/exercicios/:id", ExercicioController.index)
  .get("/exercicios", ExercicioController.getExercicios)
  .post("/exercicios", ExercicioController.store)
  .put("/exercicios/foto/:id", ExercicioController.updateFoto)
  .delete("/exercicios/:id", ExercicioController.deletaExercicio);

routes
  .get("/treinos/filtro-nome/:nome", TreinoController.show)
  .get("/treinos", TreinoController.getTreinos)
  .post("/adicionaTreinos", TreinoController.addTreino)
  .get("/treinos/:id", TreinoController.idTreino)
  .delete("/treinos/:id", TreinoController.deleteTreino);

routes
  .get(
    "/treino_has_exercicio/:id",
    TreinoHasExercicioController.listaTreinoExercicio
  )
  .post(
    "/treino_has_exercicio/:id",
    TreinoHasExercicioController.adicionaExercicio
  );

routes
  .get("/usuarios", UsuarioController.index)
  .post("/usuarios", UsuarioController.store)
  .post("/login", UsuarioController.login)
  .put("/email/:id", UsuarioController.update)
  .delete("/usuarios/:id", UsuarioController.deletaUsuario);

module.exports = routes;
