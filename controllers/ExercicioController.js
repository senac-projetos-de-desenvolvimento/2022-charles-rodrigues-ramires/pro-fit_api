const knex = require("../database/dbConfig");

module.exports = {
  //index listagem
  //store/create inclusao
  //update alteração
  //show obter 1 registro
  //destroy exclusao

  //Busca exercicio por id
  async index(req, res) {
    const { id } = req.params;
    const novo = await knex("exercicios").where("exercicioId", id);
    if (novo.length === 0) {
      res.status(400).json({ erro: "Exercício não encontrado." });
    } else {
      res.status(200).json(novo);
    }
  },

  //Adiciona um novo exercicio
  async store(req, res) {
    const { nomeExercicio, serie, descanso, repeticao } = req.body;

    if (!nomeExercicio || !serie || !descanso || !repeticao) {
      res.status(400).json({ erro: "Preencha os dados corretamente." });
      return;
    }

    try {
      const novo = await knex("exercicios").insert({
        nomeExercicio,
        serie,
        descanso,
        repeticao,
      });
      res.status(201).json("Exercicio inserido com sucesso.");
    } catch (error) {
      res.status(400).json({ erro: error.message });
    }
  },

  //Lista todos os exercicios
  async getExercicios(req, res) {
    const novo = await knex("exercicios");
    res.status(200).json(novo);
  },

  //Adiciona foto/altera foto
  async updateFoto(req, res) {
    const { id } = req.params;
    const { foto } = req.body;

    const data = await knex("exercicios")
      .where("exercicioId", id)
      .select("foto");
    const novo = data[0];

    if (!novo) {
      res.status(400).json({ erro: "Exercício não encontrado." });
    } else {
      try {
        await knex("exercicios")
          .where("exercicioId", id)
          .update({ foto: foto });
        res.send("Foto adicionada com sucesso.");
      } catch (error) {
        res.status(400).json({ erro: error.message });
      }
    }
  },

  async deletaExercicio(req, res) {
    try {
      const { id } = req.params;

      const delExercicio = await knex("exercicios")
        .del()
        .where("exercicios.exercicioId", id);
      if (!delExercicio) {
        throw new Error("Não existe exercício com esse id.");
      }
      res.status(200).send("Exercicio apagado com sucesso.");
    } catch (error) {
      res.send({ erro: error.message });
    }
  },
};
