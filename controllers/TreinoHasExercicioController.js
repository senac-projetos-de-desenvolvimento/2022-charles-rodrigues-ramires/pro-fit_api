const knex = require("../database/dbConfig");

module.exports = {
  async listaTreinoExercicio(req, res) {
    const { id } = req.params;

    const novo = await knex
      .select(
        "te.id",
        "te.treinoId",
        "te.exercicioId",
        "t.nomeTreino as nomeTreino",
        "e.nomeExercicio as nomeExercicio",
        "e.serie  as serie",
        "e.descanso as descanso",
        "e.repeticao as repetições"
      )
      .from("treino_has_exercicio as te")
      .innerJoin("treinos as t", "te.treinoId", "=", "t.treinoId")
      .innerJoin("exercicios as e", "te.exercicioId", "=", "e.exercicioId")
      .where("t.treinoId", id);

    res.status(200).json(novo);
  },

  async adicionaExercicio(req, res) {
    const { id } = req.params;
    const treinoId = await knex("treinos")
      .where("treinoId", id)
      .select("treinoId");
    const { exercicioId } = req.body;

    const novo = await knex("exercicios")
      .where("exercicioId", exercicioId)
      .select("exercicioId");

    console.log(novo);
    console.log(treinoId);
    if (!exercicioId) {
      res.status(400).json({ erro: "Envie os dados corretamente." });
      return;
    }

    try {
      const exercicios = await knex("treino_has_exercicio").insert(
        { treinoId: 1 },
        { exercicioId: 2 }
      );
    } catch (error) {
      res.status(400).json({ erro: error.message });
    }
  },
};
