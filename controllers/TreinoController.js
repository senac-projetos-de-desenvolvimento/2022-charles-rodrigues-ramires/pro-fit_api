const knex = require("../database/dbConfig");

module.exports = {
  //Lista treino filtrando pelo nome
  async show(req, res) {
    try {
      const { nome } = req.params;
      const novo = await knex("treinos").where(
        "nomeTreino",
        "like",
        `%${nome}%`
      );
      if (!novo) {
        throw new Error("Não existe treino com esse nome.");
      }
      res.status(200).json(novo);
    } catch {
      res.send("Algo deu errado.");
    }
  },

  //Lista Treinos filtrando pelo ID
  async idTreino(req, res) {
    try {
      const { id } = req.params;
      const novo = await knex("treinos").where("treinoId", id);
      console.log(novo);
      if (!novo) {
        throw new Error("Não existe treino com esse id.");
      }
      res.status(200).json(novo[0]);
    } catch {
      res.send("Erro!");
    }
  },

  //Adiciona um treino novo
  async addTreino(req, res) {
    const { nomeTreino, dificuldade, avaliacao } = req.body;

    if (!nomeTreino || !dificuldade) {
      res.status(400).json({ erro: "Preencha os dados corretamente." });
      return;
    }

    try {
      const novo = await knex("treinos").insert({
        nomeTreino,
        dificuldade,
        avaliacao,
      });
      res.status(201).json("Treino inserido com sucesso.");
    } catch (error) {
      res.status(400).json({ erro: error.message });
    }
  },

  //Lista todos os treinos
  async getTreinos(req, res) {
    const novo = await knex("treinos");
    res.status(200).json(novo);
  },

  //Deleta treino
  async deleteTreino(req, res) {
    try {
      const { id } = req.params;

      const delTreino = await knex("treinos")
        .del()
        .where("treinos.treinoId", id);
      if (!delTreino) {
        throw new Error("Não existe nenhum treino com esse id.");
      }
      res.status(200).send("Treino apagado com sucesso.");
    } catch (error) {
      res.send({ erro: error.message });
    }
  },
};
