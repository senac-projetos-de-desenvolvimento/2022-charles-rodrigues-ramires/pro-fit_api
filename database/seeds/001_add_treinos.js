exports.seed = async function (knex) {
  // Deletes ALL existing entries
  await knex("treinos").del();
  await knex("treinos").insert([
    {
      nomeTreino: "Treino de peito, ombro e triceps",
      dificuldade: "iniciante",
    },
    { nomeTreino: "Treino de costas e biceps", dificuldade: "intermediario" },
    { nomeTreino: "Treino de inferiores", dificuldade: "avançado" },
  ]);
};
