/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.seed = async function (knex) {
  // Deletes ALL existing entries
  await knex("exercicios").del();
  await knex("exercicios").insert([
    {
      nomeExercicio: "Supino Reto",
      serie: "3x10",
      descanso: "60s",
      repeticao: "3",
      foto: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSNy6kVNjYD8mmn0ANdsq4z1ZDAp7VTWJL53A&usqp=CAU",
    },
    {
      nomeExercicio: "Rosca direta",
      serie: "3x10",
      descanso: "60s",
      repeticao: "3",
    },
    {
      nomeExercicio: "Puxada de Frente",
      serie: "3x10",
      descanso: "60s",
      repeticao: "4",
    },
    {
      nomeExercicio: "Triceps Corda",
      serie: "3x10",
      descanso: "60s",
      repeticao: "3",
    },
    {
      nomeExercicio: "Agachamento",
      serie: "3x10",
      descanso: "60s",
      repeticao: "3",
    },
  ]);
};
