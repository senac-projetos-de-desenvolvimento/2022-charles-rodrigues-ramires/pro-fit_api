exports.up = (knex) => {
  return knex.schema.createTable("exercicios", (table) => {
    table.increments("exercicioId").primary();
    table.string("nomeExercicio", 60).notNullable();
    table.string("serie", 4).notNullable();
    table.string("descanso", 4).notNullable();
    table.string("repeticao", 45).notNullable();
    table.string("foto");

    table.timestamps(true, true);
  });
};

exports.down = (knex) => {
  return knex.schema.dropTable("exercicios");
};
