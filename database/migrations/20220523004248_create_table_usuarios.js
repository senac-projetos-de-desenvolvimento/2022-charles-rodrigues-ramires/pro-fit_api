exports.up = (knex) => {
  return knex.schema.createTable("usuarios", (table) => {
    table.increments("usuarioId").primary();
    table.string("nome", 60).notNullable();
    table.string("email", 120).unique().notNullable();
    table.string("senha", 60).notNullable();
    table.string("funcao", 60).notNullable();

    table.timestamps(true, true);
  });
};

exports.down = (knex) => {
  return knex.schema.dropTable("usuarios");
};
