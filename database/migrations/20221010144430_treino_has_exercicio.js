exports.up = function (knex) {
  return knex.schema.createTable("treino_has_exercicio", (table) => {
    table.increments("id").primary();

    table.integer("treinoId").unsigned().notNullable();
    table
      .foreign("treinoId")
      .references("treinos.treinoId")
      .onDelete("restrict")
      .onUpdate("cascade");

    table.integer("exercicioId").unsigned().notNullable();
    table
      .foreign("exercicioId")
      .references("exercicios.exercicioId")
      .onDelete("restrict")
      .onUpdate("cascade");
  });
};

exports.down = function (knex) {
  return knex.schema.dropTable("treinos_has_exercicio");
};
