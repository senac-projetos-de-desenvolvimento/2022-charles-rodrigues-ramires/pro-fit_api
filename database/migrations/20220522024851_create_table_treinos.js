exports.up = (knex) => {
  return knex.schema.createTable("treinos", (table) => {
    table.increments("treinoId").primary();
    table.string("nomeTreino", 60).notNullable();
    table.string("dificuldade", 60).notNullable();
    table.integer("avaliacao").unsigned();

    table.timestamps(true, true);
  });
};

exports.down = (knex) => {
  return knex.schema.dropTable("treinos");
};
